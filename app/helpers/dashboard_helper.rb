module DashboardHelper
  def short_meeting_title(meeting)
    "#{l(meeting.scheduled_at, format: :short)} - #{meeting.title.truncate(14)}"
  end

  def long_meeting_title(meeting)
    "Room: #{meeting.room.title} created by #{meeting.user.email} at #{l(meeting.scheduled_at, format: :short)} - #{meeting.title.truncate(14)}"
  end
end
