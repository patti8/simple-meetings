class MeetingsController < ApplicationController
  include CableReady::Broadcaster

  def index
    date_to_start = !params[:start_date].blank? ? params[:start_date].to_date.beginning_of_month : Date.today.beginning_of_month
    date_to_end = date_to_start.end_of_month
    @meetings = Meeting.where("day between ? and ?", date_to_start, date_to_end).all
    @meeting = Meeting.new
  end

  def create
    date_start = params[:date_start]
    date_start = Date.today.change(
      year: date_start["(1i)"].to_i,
      month: date_start["(2i)"].to_i,
      day: date_start["(3i)"].to_i
    )

    date_end = params[:date_end]
    date_end = Date.today.change(
      year: date_end["(1i)"].to_i,
      month: date_end["(2i)"].to_i,
      day: date_end["(3i)"].to_i
    )

    days = (date_end - date_start).to_i
    if days == 0
      dates = [date_start]
    else
      dates = (0..days).collect do |day|
        date_start + day.day
      end
    end

    meeting = current_user.meetings.build(meeting_params)
    meeting.user_id = current_user.id
    dates.each do |date|
      new_meeting = current_user.meetings.build(
        user_id: meeting.user_id,
        room_id: meeting.room_id,
        title: meeting.title,
        scheduled_at: meeting.scheduled_at,
        day: date
      )
      new_meeting.save
    end

    meetings = Meeting.where("day between ? and ?", dates.first.beginning_of_month, dates.first.end_of_month).all

    cable_ready["meetings"].inner_html(
      selector: "#calendar",
      html: render_to_string(partial: "calendar", locals: {global_meetings: meetings})
    )
    cable_ready.broadcast

    redirect_to meetings_path(start_date: dates.first.beginning_of_month)
  end

  private
    def meeting_params
      params.require(:meeting).permit(:title, :room_id, :scheduled_at)
    end
end
