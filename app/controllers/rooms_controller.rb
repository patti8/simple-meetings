class RoomsController < ApplicationController
  include CableReady::Broadcaster
  before_action :set_room, :only_if_exists, only: [:edit, :update, :destroy]
  before_action :set_rooms, only: [:edit,:index]

  def index
    @room = Room.new
  end

  def create
    Room.create(room_params)
    dispatch_update_room_list
    redirect_to rooms_path
  end

  def edit
    render action: :index
  end

def update
  room = Room.find(params[:id])
  dispatch_update_room_list if room.update_attributes(room_params)

  redirect_to rooms_path
end


  def destroy
    @room.destroy
    cable_ready["rooms"].remove(
      selector: "#room-#{@room.id}"
    )
    cable_ready.broadcast
    redirect_to rooms_path
  end

  private
    def set_rooms
      @rooms = Room.all
    end

    def set_room
      @room = Room.find(params[:id])
    end

    def only_if_exists
      redirect_to rooms_path unless @room.present?
    end

    def room_params
      params.require(:room).permit(:title)
    end

    def dispatch_update_room_list
      cable_ready["rooms"].insert_adjacent_html(
        selector: "#rooms",
        position: "afterbegin",
        html: render_to_string(partial: "room", collection: Room.all)
      )
      cable_ready.broadcast
    end
end
