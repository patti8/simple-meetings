require 'rails_helper'

RSpec.describe Room, type: :model do
  describe "create and update actions" do
    context "positive scenarios" do
      it "easy to create" do
        room = Room.create(attributes_for(:room))
        expect(room).to be_truthy
      end
    end

    context "negative scenarios" do
      it "validate presence of title" do
        room = Room.new
        expect(room.valid?).to be_falsey
        expect(room.errors_on(:title)).to include("can't be blank")
      end
    end

  end
end
