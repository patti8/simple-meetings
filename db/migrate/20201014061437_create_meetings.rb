class CreateMeetings < ActiveRecord::Migration[6.0]
  def change
    create_table :meetings do |t|
      t.references :room, null: false, foreign_key: true
      t.string :title, null: false
      t.datetime :time_to_start, null: false
      t.datetime :time_to_end, null: false

      t.timestamps
    end
  end
end
