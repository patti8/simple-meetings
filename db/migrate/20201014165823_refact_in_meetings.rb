class RefactInMeetings < ActiveRecord::Migration[6.0]
  def up
    remove_column :meetings, :time_to_start
    remove_column :meetings, :time_to_end
    add_column :meetings, :day, :date
    add_column :meetings, :scheduled_at, :time, null: false
  end

  def down
    add_column :meetings, :time_to_start, :datetime
    add_column :meetings, :time_to_end, :datetime
    remove_column :meetings, :day
    remove_column :meetings, :scheduled_at
  end
end
